//heres a javascript file for any of your needs~
console.log("Hello there");

//javascript for hamburger menu
const menu = document.querySelector(".menu");
const navItems = document.querySelectorAll(".nav");
const hamburger = document.querySelector(".hamburger");
const closeIcon = document.querySelector(".closeIcon");
const menuIcon = document.querySelector(".menuIcon");

function toggleMenu(){
    if(menu.classList.contains("showMenu")){
        menu.classList.remove("showMenu");
        closeIcon.style.display = "none";
        menuIcon.style.display = "block";
    }
    else {
        menu.classList.add("showMenu");
        closeIcon.style.display = "block";
        menuIcon.style.display = "none";
    }
}

hamburger.addEventListener("click", toggleMenu);

navItems.forEach(
    function(navItem){
        navItem.addEventListener("click", toggleMenu);
    }
)

//javascript for a slideshow carrasel
let slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n){
    showDivs(slideIndex += n);
}

function showDivs(n){
    let y;
    let x = document.getElementsByClassName("slideImage");
    if(n > x.length){
        slideIndex = 1;
    }
    if(n < 1){
        slideIndex = x.length;
    }
    for(y = 0; y < x.length; y++){
        x[y].style.display = "none";
    }
    x[slideIndex-1].style.display = "block";
}